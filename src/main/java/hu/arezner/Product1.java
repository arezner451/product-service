/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.arezner;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author attila rezner <attila.rezner@gmail.com>
 */
@WebService(serviceName = "Product1")
@Stateless()
public class Product1 {

    /**
     * Web service operation
     * @param requestXmlString
     * @return responseXmlString
     */
    @WebMethod(operationName = "getProductList")
    public String getProductList(@WebParam(name = "requestXmlString") String requestXmlString) {
        //TODO write your implementation code here:
        String responseXmlString;

        int customerId = getProductsByFeatures(requestXmlString);
        responseXmlString = getProductsFromPortfolio(customerId);
        
        return responseXmlString;
    } 
    
    private int getProductsByFeatures(String xmlString) {
        System.out.println("request: " +xmlString);
        return 1;
    }
    
    private String getProductsFromPortfolio(int customerId) {
        return "product list by features: " +String.valueOf(customerId) +" from Portfolio";
    }
    
}
